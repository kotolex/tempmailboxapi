API for working with temporary email accounts
=====================

At the moment, works only with:

* postshift (boxes like <text>@post-shift.ru) <https://post-shift.ru/api/>

* guerrilla mail (boxes like <text>@guerrillamailblock.com) <https://www.guerrillamail.com/>

This API is designed to automate testing, for example, to verify registration and password recovery, when
it is not necessary to create your own SMTP server. Post-shift mailboxes are created for a short period of time (10 minutes), there are special methods to extend it.
You can check the letters, read their contents, delete letters

Example:

* create new mailbox

    ```temp_mail = PostShiftMailBox() or temp_mail = GuerillaMailBox()```
    
    ```temp_mail.mailbox() #to get mailbox name```
    
    ```temp_mail.api_key() #to get api key```

* use already created mail box (make sure it still alive for postshift)

    ```temp_mail = PostShiftMailBox( 'd6be5cce6245b61552cdc5201e0488b2' , 'pbrqvjxw90@post-shift.ru')```
    

* check lifetime of mailbox (in seconds) for post-shift API

    ```temp_mail.lifetime()```
    

* prolong lifetime up to 600 seconds for post-shift API

    ```temp_mail.prolong_lifetime()```
    

* get all mails (in dict)

    ```temp_mail.get_all_mails()```
    

* get text of the mail by id (id in the mailbox)

    ```temp_mail.get_email_content_by_id(1)```

