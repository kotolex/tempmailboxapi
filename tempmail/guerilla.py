import requests
from tempmail.abstract import TempMailBox


class GuerillaMailBox(TempMailBox):
    """
    Class for using Guerilla API. Fast API, but there are some rules
    for deleting iframes, js-content etc. from letter content
    """
    #TODO set_name

    def __init__(self, mailbox=None, key=None):
        self.__PATH = 'https://api.guerrillamail.com/ajax.php?'
        super().__init__(mailbox, key)

    def _make_request(self, func, **kwargs):
        self._url = f'{self.__PATH}f={func}'
        for key, value in kwargs.items():
            self._url += f'&{key}={value}'
        resp = requests.get(self._url)
        if 'false' in resp.text:
            return resp.text
        if 'error' in resp.json():
            raise ValueError(resp.json()['error'])
        return resp.json()

    def _create_mailbox(self):
        params = {'lang': 'en', 'site': 'guerrillamail.com', 'ip': '127.0.0.1', 'agent': 'Python_console'}
        resp = self._make_request('get_email_address', **params)
        return resp['sid_token'], resp['email_addr']

    def get_all_mails(self) -> list:
        return self._make_request('get_email_list', **{'offset': '0', 'sid_token': self._key})['list']

    def check_for_new_mails(self) -> list:
        """
        Returns only new mails since last check, so list can be empty
        :return: list
        """
        return self._make_request('check_email', **{'seq': '0', 'sid_token': self._key})

    def is_mail_from_addr_exists(self, email_addr: str) -> bool:
        return bool(self.get_all_mails_from_addr(email_addr))

    def get_all_mails_from_addr(self, email_addr: str) -> list:
        return [line for line in self.get_all_mails() if email_addr in line['mail_from']]

    def get_email_content_by_id(self, email_id: int) -> str:
        resp = self._make_request('fetch_email', **{'email_id': email_id, 'sid_token': self._key})
        if 'false' in resp:
            raise IndexError(f'No letter with id {email_id}')
        return resp.json()['mail_body']

    def delete_mail_by_id(self, email_id):
        self._make_request('del_email', **{'email_ids[]': email_id, 'sid_token': self._key})
