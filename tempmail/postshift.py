import requests
from tempmail.abstract import TempMailBox


class PostShiftMailBox(TempMailBox):
    """
    Class for using post-shift.ru mail API. All mail boxes are maximum 600 seconds to live, except named boxes -
    they live for 1 hour
    """
    #TODO set_name

    def __init__(self, mailbox=None, key=None):
        self.__PATH = 'https://postshift.ru/api.php?'
        super().__init__(mailbox, key)

    def _create_mailbox(self):
        resp = self._make_request('new')
        return resp['key'], resp['email']

    def _make_request(self, action, response_type='json', params_dict=None):
        self._url = f'{self.__PATH}action={action}'
        if response_type:
            self._url += f"&type={response_type}"
        if params_dict:
            for key, value in params_dict.items():
                self._url += f"&{key}={value}"
        resp = requests.get(self._url)
        text = resp.text if 'text' in resp.headers['Content-Type'] else resp.json()
        if 'error' in text and 'not_found' in text['error']:
            raise ValueError(text['error'])
        return text

    def lifetime(self) -> int:
        """
        Returns the lifetime for the mail box in seconds, maximum is 600 seconds
        :return: time in seconds
        """
        return self._make_request("livetime", params_dict={'key': self.api_key()})['livetime']

    def prolong_lifetime(self):
        """
        Prolong life of the mail box - resets it to 600 seconds
        """
        self._make_request("update", params_dict={'key': self.api_key()})

    def get_all_mails(self) -> dict:
        answer = self._make_request("getlist", params_dict={'key': self.api_key()})
        return dict() if 'error' in answer else answer

    def get_email_content_by_id(self, email_id: int) -> str:
        dict = {'key': self.api_key(), 'id': str(email_id), 'forced': '1'}
        return self._make_request("getmail", response_type='', params_dict=dict)

    def get_last_mail_text(self) -> str:
        """
        Returns text of last received mail
        :return: str
        """
        mail_list = [line['id'] for line in self.get_all_mails()]
        if not mail_list:
            raise RuntimeError('No letters in mail box')
        return self.get_email_content_by_id(max(mail_list))

    def get_ids_list_by_from(self, from_value: str) -> list:
        """
        Returns the list with all letter id's with current From field
        :param from_value: address field From
        :return: list
        """
        return self._get_ids_by_field(from_value)

    def get_ids_list_by_subject(self, subject_value: str) -> list:
        """
        Returns the list with all letter id's with current subject
        :param subject_value:
        :return: list
        """
        return self._get_ids_by_field(subject_value, False)

    def _get_ids_by_field(self, value: str, is_from=True) -> list:
        field = 'from' if is_from else 'subject'
        return [line['id'] for line in self.get_all_mails() if value in line[field]]

    def clear_mailbox(self):
        """
        Deletes all emails in box
        :return: OK
        """
        resp = self._make_request('clear', params_dict={'key': self.api_key()})
        assert resp['clear'] == 'ok'  # API cant send anything else
