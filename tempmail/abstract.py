from abc import ABCMeta, abstractmethod


class TempMailBox(metaclass=ABCMeta):

    def __init__(self, mailbox=None, key=None):
        """
        If both parameters empty - creates new mail box. Otherwise both mailbox and key (api token) must present
        :param mailbox: name of mailbox
        :type str
        :param key: key or api token for mail API
        :type str
        """
        if not any([key, mailbox]):
            self._key, self._mailbox = self._create_mailbox()
            return
        if not all([key, mailbox]):
            raise ValueError("Api key and mailbox must be specified!")
        self._key, self._mailbox = key, mailbox

    def mailbox(self) -> str:
        """
        Returns current mailbox address
        :return: mailbox
        """
        return self._mailbox

    def api_key(self) -> str:
        """
        Returns current api key or api token for API
        :return: api key
        """
        return self._key

    def __str__(self):
        return f'{self.__class__.__name__} api_key: {self._key} mail: {self._mailbox}'

    @abstractmethod
    def _create_mailbox(self) -> tuple:
        pass

    @abstractmethod
    def get_all_mails(self) -> dict:
        """
        Get a dictionary of all letters from mailbox. If mailbox is empty -returns empty dict
        :return: all letters
        :type: dict
        """
        pass

    @abstractmethod
    def get_email_content_by_id(self, email_id: int) -> str:
        """
        Return text content of letter with id
        :param email_id:
        :return: content of the letter
        """
        pass
