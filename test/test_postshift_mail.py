import pytest
from tempmail.postshift import PostShiftMailBox

wrong_email = 'test@post-shift.ru'
wrong_key = 'wrong_key'
temp_mail = None


def test_fail_if_init_witout_mail():
    with pytest.raises(ValueError):
        PostShiftMailBox(wrong_key)


def test_lifetime_must_fail_if_wrong_key():
    with pytest.raises(ValueError):
        PostShiftMailBox(wrong_key, wrong_email).lifetime()


def test_update_fail_if_wrong_key():
    with pytest.raises(ValueError):
        PostShiftMailBox(wrong_key, wrong_email).prolong_lifetime()


def test_ids_must_fail_if_wrong_key():
    with pytest.raises(ValueError):
        PostShiftMailBox(wrong_key, wrong_email).get_ids_list_by_from(wrong_email)


def test_getid_fail_if_wrong_key():
    assert 'not found' in PostShiftMailBox(wrong_key, wrong_email).get_email_content_by_id(1)


@pytest.mark.run(run=2)
def test_must_create_mail():
    global temp_mail
    temp_mail = PostShiftMailBox()
    assert temp_mail.mailbox()
    assert temp_mail.api_key()


@pytest.mark.run(run=3)
def test_must_return_empty_if_no_mails():
    assert temp_mail.get_all_mails() == {}


@pytest.mark.run(run=3)
def test_must_return_empty_ids_if_no_mails():
    assert temp_mail.get_ids_list_by_from(wrong_email) == []


@pytest.mark.run(run=3)
def test_must_return_empty_subject_ids_if_no_mails():
    assert temp_mail.get_ids_list_by_subject(wrong_key) == []


@pytest.mark.run(run=4)
def test_must_return_bigger_than_zero_lifetime():
    time = temp_mail.lifetime()
    assert time > 0


@pytest.mark.run(run=5)
def test_must_prolong_lifetime():
    start = temp_mail.lifetime()
    temp_mail.prolong_lifetime()
    assert temp_mail.lifetime() > start


@pytest.mark.run(run=5)
def test_getid_fail_if_no_mails():
    assert 'not found' in temp_mail.get_email_content_by_id(1)


@pytest.mark.run(run=6)
def test_must_fail_last_mail_if_no_mails():
    with pytest.raises(RuntimeError):
        temp_mail.get_last_mail_text()


@pytest.mark.run(run=6)
def test_clear_mail_must_works_even_if_no_emails():
    assert temp_mail.clear_mailbox() is None
