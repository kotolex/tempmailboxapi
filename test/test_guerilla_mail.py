import pytest
from tempmail.guerilla import GuerillaMailBox

wrong_email = 'test@guerrillamailblock.com'
support_email = 'no-reply@guerrillamail.com'
wrong_key = 'wrong_key'
temp_mail = None


def test_must_fail_if_no_key():
    with pytest.raises(ValueError):
        GuerillaMailBox(mailbox=wrong_email)


def test_must_fail_if_no_mailbox():
    with pytest.raises(ValueError):
        GuerillaMailBox(key=wrong_key)


def test_must_create_mailbox():
    global temp_mail
    temp_mail = GuerillaMailBox()
    assert temp_mail.mailbox()
    assert temp_mail.api_key()


def test_get_all_mails_not_empty():
    assert temp_mail.get_all_mails() != []


def test_get_check_mails_not_empty_in_first_time():
    assert temp_mail.check_for_new_mails() != []


def test_mail_from_support_exists():
    assert temp_mail.is_mail_from_addr_exists(support_email)


@pytest.mark.run(run=3)
def test_must_fail_if_wrong_id():
    with pytest.raises(IndexError):
        temp_mail.get_email_content_by_id(0)


@pytest.mark.run(run=4)
def test_must_returns_non_empty_list_for_support():
    assert temp_mail.get_all_mails_from_addr(support_email) != []


@pytest.mark.run(run=5)
def test_should_fail_if_wrong_key():
    temp_mail = GuerillaMailBox(wrong_email, wrong_key)
    with pytest.raises(ValueError):
        temp_mail.check_for_new_mails()
